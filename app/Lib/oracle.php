<?php

namespace App\Lib;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class oracle
{
    private $db;

    public function __construct()
    {
        $this->db = 'oracle';
    }

    public function setConnection($host, $port, $username, $password, $sid)
    {
        Config::set('database.connections.oracle.host', $host);
        Config::set('database.connections.oracle.port', $port);
        Config::set('database.connections.oracle.username', $username);
        Config::set('database.connections.oracle.password', $password);
        Config::set('database.connections.oracle.service_name', $sid);

        try
        {
            DB::connection()->getPdo($this->db);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function query($query)
    {
        try
        {
            DB::connection($this->db)->select($query);
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }
}
