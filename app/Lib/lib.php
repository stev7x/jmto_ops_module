<?php

namespace App\Lib;

class Lib
{
    public function responseData($success, $message, $data, $count ,$http_response)
    {
        return response()->json([
            'success' => $success,
            'message' => $message,
            'count' => $count,
            'data' => $data
        ], $http_response);
    }

    public function response($success, $message, $return_file, $file_url, $http_response)
    {
        return response()->json([
            'success' => $success,
            'message' => $message,
            'return_file' => $return_file,
            'file_url' => $file_url
        ], $http_response);
    }

    public function dateNow() {
        return date('Y-m-d H:i:s');
    }

    public function responseBroadcast($success, $message, $return_file, $file_url)
    {
        $data = array(
            'success' => $success,
            'message' => $message,
            'return_file' => $return_file,
            'file_url' => $file_url);

        $message = json_encode($data);

        $ch = curl_init('https://telebot.jmto.co.id/coreapi');

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($message))
        );

        echo curl_exec($ch);
    }
}
