<?php namespace App\Http\Controllers;

use App\Cabang;
use App\Lib\Lib;

class CabangsController extends Controller {

    protected $lib;

    public function __construct()
    {
        $this->lib = new Lib();
    }

    public function all()
    {
        $data = Cabang::all();
        return $this->lib->responseData(TRUE, 'List Data Cabang', $data->count(), $data,200);
    }
}
