<?php namespace App\Http\Controllers;

use App\Cabang;
use App\Exports\AT4Export;
use App\Exports\AntrianExport;
use App\Exports\CekFSExport;
use App\Exports\CekKonsistensiExport;
use App\Exports\DataCabangExport;
use App\Exports\TableSpaceExport;
use App\Lib\Lib;
use Illuminate\Http\Request;
use App\Exports\RekapExport;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class JmtoModules extends Controller {

    protected $lib;

    public function __construct()
    {
        $this->lib = new Lib();
    }

    public function allCabang(Request $request)
    {
        $dataCabang = Cabang::all();
        $index      = 0;

        foreach ($dataCabang as $key => $value) {
           $data['data'][$index]['nama_cabang'] = $value['nama_cabang'];
           $index++;
        }

        return json_encode($data, true);
    }

    public function at4(Request $request)
    {
        if (empty($request['tgl_report']))
        {
            return $this->lib->response(TRUE, "tgl_report cant be empty", FALSE, FALSE, 200);
        }

        if ($dataCabang = Cabang::all()->firstWhere('nama_cabang', strtoupper($request['nama_cabang'])))
        {
            Config::set('database.connections.oracle.host', $dataCabang['host']);
            Config::set('database.connections.oracle.port', $dataCabang['port']);
            Config::set('database.connections.oracle.username', $dataCabang['username']);
            Config::set('database.connections.oracle.password', $dataCabang['password']);
            Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

            $query = "SELECT ROW_NUMBER() OVER (ORDER BY IDGERBANG) AS NO, tgl_report, idcabang, idgerbang, shift, SUM (total_transaksi) total_transaksi, SUM (total_rupiah) total_rupiah
                      FROM t_data_atb4
                      WHERE tgl_report = " . $request['tgl_report'];
            $fileName = "DataAT4_" . $request['nama_cabang'] . "_tglreport" . $request['tgl_report'];
            $fileCaption = "Data AT4 Cabang " . strtoupper($request['nama_cabang']) . "\nTanggal Report " . $request['tgl_report'];

            if (!empty($request['idcabang']) || $request['idcabang'] != 0)
            {
                $query = $query . " AND idcabang = " . $request['idcabang'];
                $fileName = $fileName . "_idcabang" . $request['idcabang'];
                $fileCaption = $fileCaption . "\nID Cabang " . $request['idcabang'];
            }

            if (!empty($request['idgerbang']) || $request['idgerbang'] != 0)
            {
                $query = $query . " AND idgerbang = " . $request['idgerbang'];
                $fileName = $fileName . "_idgerbang" . $request['idgerbang'];
                $fileCaption = $fileCaption . "\nID Gerbang " . $request['idgerbang'];
            }

            if (!empty($request['shift']) || $request['shift'] != 0)
            {
                $query = $query . " AND shift = " . $request['shift'];
                $fileName = $fileName . "_shift" . $request['shift'];
                $fileCaption = $fileCaption . "\nShift " . $request['shift'];
            }

            $query = $query . " AND insert_status = 1 AND aktiv = 1 GROUP BY tgl_report, idcabang, idgerbang, shift ORDER BY idgerbang";
            $fileNameEx = $fileName . ".xlsx";

            try
            {
                DB::connection('oracle')->getPdo();
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }

            try
            {
                if ($response = DB::connection('oracle')->select($query))
                {
                    if ((new AT4Export($response))->store($fileNameEx))
                    {
//                        $filePath = storage_path('app');
//                        exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                        $fileUrl = url('download/' . $fileName . ".zip");

                        return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                    }
                    else
                    {
                        return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                    }
                }
                else
                {
                    return $this->lib->response(FALSE, $fileCaption . "\nis empty", FALSE, FALSE, 200);
                }
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }
        }
        else
        {
            return $this->lib->response(FALSE, "Cant find cabang " . $request['nama_cabang'], FALSE, FALSE, 200);
        }
    }

    public function cekFS(Request $request)
    {
        if (empty($request['tgl_report']))
        {
            return $this->lib->response(TRUE, "tgl_report cant be empty", FALSE, FALSE, 200);
        }

        if ($dataCabang = Cabang::all()->firstWhere('nama_cabang', strtoupper($request['nama_cabang'])))
        {
            Config::set('database.connections.oracle.host', $dataCabang['host']);
            Config::set('database.connections.oracle.port', $dataCabang['port']);
            Config::set('database.connections.oracle.username', $dataCabang['username']);
            Config::set('database.connections.oracle.password', $dataCabang['password']);
            Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

            $query = "SELECT ROW_NUMBER() OVER (ORDER BY kode_bank) AS NO, kode_bank, tgl_report, status, COUNT (*) tot_data, TO_CHAR (submit_date, 'yyyymmddhh24') submit_date
                      FROM v_reqdet_allbank
                      WHERE tgl_report = " . $request['tgl_report'] . " AND status = " . $request['status'] . "
                      GROUP BY kode_bank, tgl_report, status, TO_CHAR (submit_date, 'yyyymmddhh24')";
            $fileName = "CekCreateFS_" . $request['nama_cabang'] . "_tglreport" . $request['tgl_report'] . "_status" . $request['status'];
            $fileCaption = "Cek Create FS " . strtoupper($request['nama_cabang']) . "\nTanggal Report " . $request['tgl_report'] . "\nStatus " . $request['status'];

            $fileNameEx = $fileName . ".xlsx";

            try
            {
                DB::connection('oracle')->getPdo();
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }

            try
            {
                if ($response = DB::connection('oracle')->select($query))
                {
                    if ((new CekFSExport($response))->store($fileNameEx))
                    {
//                        $filePath = storage_path('app');
//                        exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                        $fileUrl = url('download/' . $fileName);

                        return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                    }
                    else
                    {
                        return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                    }
                }
                else
                {
                    return $this->lib->response(FALSE, $fileCaption . "\nis empty", FALSE, FALSE, 200);
                }
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }
        }
        else
        {
            return $this->lib->response(FALSE, "Cant find cabang " . $request['nama_cabang'], FALSE, FALSE, 200);
        }
    }

    public function cekKonsistensi(Request $request)
    {
        if (empty($request['tgl_report']))
        {
            return $this->lib->response(TRUE, "tgl_report cant be empty", FALSE, FALSE, 200);
        }

        if ($dataCabang = Cabang::all()->firstWhere('nama_cabang', strtoupper($request['nama_cabang'])))
        {
            Config::set('database.connections.oracle.host', $dataCabang['host']);
            Config::set('database.connections.oracle.port', $dataCabang['port']);
            Config::set('database.connections.oracle.username', $dataCabang['username']);
            Config::set('database.connections.oracle.password', $dataCabang['password']);
            Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

            $query = "SELECT ROW_NUMBER() OVER (ORDER BY tgl_report) AS NO, tgl_report, kode_bank, kode_cabang, kode_gerbang, shift, lalin_att4, lalin_jurnal, lalin_fs, lalin_rfs_ok, lalin_rfs_nok
                      FROM rekap_fs_rfs_rk
                      WHERE tgl_report = '" . $request['tgl_report'] . "'
                      AND   (lalin_att4 <> lalin_jurnal
                      OR     lalin_jurnal <> lalin_fs
                      OR     lalin_fs <> lalin_rfs_ok + lalin_rfs_nok)
                      AND   lalin_rfs_ok <> 0
                      ORDER BY tgl_report, kode_bank, kode_cabang, kode_gerbang, shift";
            $fileName = "CekKonsistensi_" . $request['nama_cabang'] . "_tglreport" . $request['tgl_report'];
            $fileCaption = "Cek Konsistensi " . strtoupper($request['nama_cabang']) . "\nTanggal Report " . $request['tgl_report'];

            $fileNameEx = $fileName . ".xlsx";

            try
            {
                DB::connection('oracle')->getPdo();
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }

            try
            {
                if ($response = DB::connection('oracle')->select($query))
                {
                    if ((new CekKonsistensiExport($response))->store($fileNameEx))
                    {
//                        $filePath = storage_path('app');
//                        exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                        $fileUrl = url('download/' . $fileName);

                        return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                    }
                    else
                    {
                        return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                    }
                }
                else
                {
                    return $this->lib->response(FALSE, $fileCaption . "\nis empty", FALSE, FALSE, 200);
                }
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }
        }
        else
        {
            return $this->lib->response(FALSE, "Cant find cabang " . $request['nama_cabang'], FALSE, FALSE, 200);
        }
    }

    public function dataCabang()
    {
        $fileName = "DataCabang";
        $fileCaption = "Data Cabang";

        $fileNameEx = $fileName . ".xlsx";

        try
        {
            if ($response = Cabang::all())
            {
                if ((new DataCabangExport($response))->store($fileNameEx))
                {
//                    $filePath = storage_path('app');
//                    exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                    $fileUrl = url('download/' . $fileName);

                    return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                }
                else
                {
                    return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                }
            }
            else
            {
                return $this->lib->response(FALSE, "Data not found", FALSE, FALSE, 200);
            }
        }
        catch (\Exception $e)
        {
            return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
        }
    }

    public function antrian(Request $request)
    {
        if (empty($request['periode']))
        {
            return $this->lib->response(TRUE, "periode cant be empty", FALSE, FALSE, 200);
        }

        if ($dataCabang = Cabang::all()->firstWhere('nama_cabang', strtoupper($request['nama_cabang'])))
        {
            Config::set('database.connections.oracle.host', $dataCabang['host']);
            Config::set('database.connections.oracle.port', $dataCabang['port']);
            Config::set('database.connections.oracle.username', $dataCabang['username']);
            Config::set('database.connections.oracle.password', $dataCabang['password']);
            Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

            $query = "select a.periode, a.processtype_id||' - '||b.processtype PROCESSTYPE, a.processstatus_id||' - '||c.processstatus PROCESSSTATUS, count(*)
                        from t_process a
                        inner join p_processtype b on a.processtype_id = b.processtype_id
                        inner join p_processstatus c on a.processstatus_id = c.processstatus_id
                        where a.periode=" .$request['periode']. "
                        group by a.periode, a.processtype_id||' - '||b.processtype, a.processstatus_id||' - '||c.processstatus
                        order by a.periode, a.processtype_id||' - '||b.processtype, a.processstatus_id||' - '||c.processstatus";
            $fileName = "DataPeriode_" . $request['nama_cabang'] . "_periode" . $request['periode'];
            $fileCaption = "Data Periode Cabang " . strtoupper($request['nama_cabang']) . "\nPeriode " . $request['periode'];

            $fileNameEx = $fileName . ".xlsx";

            try
            {
                DB::connection('oracle')->getPdo();
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }

            try
            {
                if ($response = DB::connection('oracle')->select($query))
                {
                    if ((new AntrianExport($response))->store($fileNameEx))
                    {
//                        $filePath = storage_path('app');
//                        exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                        $fileUrl = url('download/' . $fileName);

                        return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                    }
                    else
                    {
                        return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                    }
                }
                else
                {
                    return $this->lib->response(FALSE, $fileCaption . "\nis empty", FALSE, FALSE, 200);
                }
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }
        }
        else
        {
            return $this->lib->response(FALSE, "Cant find cabang " . $request['nama_cabang'], FALSE, FALSE, 200);
        }
    }

    public function rekap(Request $request)
    {
        if (empty($request['tgl_report']))
        {
            return $this->lib->response(TRUE, "tgl_report cant be empty", FALSE, FALSE, 200);
        }
        if (empty($request['shift']))
        {
            return $this->lib->response(TRUE, "shift cant be empty", FALSE, FALSE, 200);
        }

        if ($dataCabang = Cabang::all()->firstWhere('nama_cabang', strtoupper($request['nama_cabang'])))
        {
            Config::set('database.connections.oracle.host', $dataCabang['host']);
            Config::set('database.connections.oracle.port', $dataCabang['port']);
            Config::set('database.connections.oracle.username', $dataCabang['username']);
            Config::set('database.connections.oracle.password', $dataCabang['password']);
            Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

            $query = "SELECT TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT, MDRI_L_AT4, MDRI_P_AT4, BBRI_L_AT4, BBRI_P_AT4, BBNI_L_AT4, BBNI_P_AT4, BBCA_L_AT4, BBCA_P_AT4, MDRI_L_AT4 + BBRI_L_AT4 + BBNI_L_AT4 + BBCA_L_AT4 TOT_L_AT4, MDRI_P_AT4 + BBRI_P_AT4 + BBNI_P_AT4 + BBCA_P_AT4 TOT_P_AT4, MDRI_L_JN, MDRI_P_JN, BBRI_L_JN, BBRI_P_JN, BBNI_L_JN, BBNI_P_JN, BBCA_L_JN, BBCA_P_JN, MDRI_L_JN + BBRI_L_JN + BBNI_L_JN + BBCA_L_JN TOT_L_JN, MDRI_P_JN + BBRI_P_JN + BBNI_P_JN + BBCA_P_JN TOT_P_JN, MDRI_L_RJ, MDRI_P_RJ, BBRI_L_RJ, BBRI_P_RJ, BBNI_L_RJ, BBNI_P_RJ, BBCA_L_RJ, BBCA_P_RJ, MDRI_L_RJ + BBRI_L_RJ + BBNI_L_RJ + BBCA_L_RJ TOT_L_RJ, MDRI_P_RJ + BBRI_P_RJ + BBNI_P_RJ + BBCA_P_RJ TOT_P_RJ, MDRI_L_JNRJ, MDRI_P_JNRJ, BBRI_L_JNRJ, BBRI_P_JNRJ, BBNI_L_JNRJ, BBNI_P_JNRJ, BBCA_L_JNRJ, BBCA_P_JNRJ, MDRI_L_JNRJ + BBRI_L_JNRJ + BBNI_L_JNRJ + BBCA_L_JNRJ TOT_L_JNRJ, MDRI_P_JNRJ + BBRI_P_JNRJ + BBNI_P_JNRJ + BBCA_P_JNRJ TOT_P_JNRJ, MDRI_L_FS, MDRI_P_FS, BBRI_L_FS, BBRI_P_FS, BBNI_L_FS, BBNI_P_FS, BBCA_L_FS, BBCA_P_FS, MDRI_L_FS + BBRI_L_FS + BBNI_L_FS + BBCA_L_FS TOT_L_FS, MDRI_P_FS + BBRI_P_FS + BBNI_P_FS + BBCA_P_FS TOT_P_FS, MDRI_L_RFSOK, MDRI_P_RFSOK, BBRI_L_RFSOK, BBRI_P_RFSOK, BBNI_L_RFSOK, BBNI_P_RFSOK, BBCA_L_RFSOK, BBCA_P_RFSOK, MDRI_L_RFSOK + BBRI_L_RFSOK + BBNI_L_RFSOK + BBCA_L_RFSOK TOT_L_RFSOK, MDRI_P_RFSOK + BBRI_P_RFSOK + BBNI_P_RFSOK + BBCA_P_RFSOK TOT_P_RFSOK, MDRI_L_RFSNOK, MDRI_P_RFSNOK, BBRI_L_RFSNOK, BBRI_P_RFSNOK, BBNI_L_RFSNOK, BBNI_P_RFSNOK, BBCA_L_RFSNOK, BBCA_P_RFSNOK, MDRI_L_RFSNOK + BBRI_L_RFSNOK + BBNI_L_RFSNOK + BBCA_L_RFSNOK TOT_L_RFSNOK, MDRI_P_RFSNOK + BBRI_P_RFSNOK + BBNI_P_RFSNOK + BBCA_P_RFSNOK TOT_P_RFSNOK, list_reject FROM (SELECT TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT, SUM (MDRI_L_AT4) MDRI_L_AT4, SUM (MDRI_P_AT4) MDRI_P_AT4, SUM (BBRI_L_AT4) BBRI_L_AT4, SUM (BBRI_P_AT4) BBRI_P_AT4, SUM (BBNI_L_AT4) BBNI_L_AT4, SUM (BBNI_P_AT4) BBNI_P_AT4, SUM (BBCA_L_AT4) BBCA_L_AT4, SUM (BBCA_P_AT4) BBCA_P_AT4, SUM (MDRI_L_JN) MDRI_L_JN, SUM (MDRI_P_JN) MDRI_P_JN, SUM (BBRI_L_JN) BBRI_L_JN, SUM (BBRI_P_JN) BBRI_P_JN, SUM (BBNI_L_JN) BBNI_L_JN, SUM (BBNI_P_JN) BBNI_P_JN, SUM (BBCA_L_JN) BBCA_L_JN, SUM (BBCA_P_JN) BBCA_P_JN, SUM (MDRI_L_RJ) MDRI_L_RJ, SUM (MDRI_P_RJ) MDRI_P_RJ, SUM (BBRI_L_RJ) BBRI_L_RJ, SUM (BBRI_P_RJ) BBRI_P_RJ, SUM (BBNI_L_RJ) BBNI_L_RJ, SUM (BBNI_P_RJ) BBNI_P_RJ, SUM (BBCA_L_RJ) BBCA_L_RJ, SUM (BBCA_P_RJ) BBCA_P_RJ, SUM (MDRI_L_JNRJ) MDRI_L_JNRJ, SUM (MDRI_P_JNRJ) MDRI_P_JNRJ, SUM (BBRI_L_JNRJ) BBRI_L_JNRJ, SUM (BBRI_P_JNRJ) BBRI_P_JNRJ, SUM (BBNI_L_JNRJ) BBNI_L_JNRJ, SUM (BBNI_P_JNRJ) BBNI_P_JNRJ, SUM (BBCA_L_JNRJ) BBCA_L_JNRJ, SUM (BBCA_P_JNRJ) BBCA_P_JNRJ, SUM (MDRI_L_FS) MDRI_L_FS, SUM (MDRI_P_FS) MDRI_P_FS, SUM (BBRI_L_FS) BBRI_L_FS, SUM (BBRI_P_FS) BBRI_P_FS, SUM (BBNI_L_FS) BBNI_L_FS, SUM (BBNI_P_FS) BBNI_P_FS, SUM (BBCA_L_FS) BBCA_L_FS, SUM (BBCA_P_FS) BBCA_P_FS, SUM (MDRI_L_RFSOK) MDRI_L_RFSOK, SUM (MDRI_P_RFSOK) MDRI_P_RFSOK, SUM (BBRI_L_RFSOK) BBRI_L_RFSOK, SUM (BBRI_P_RFSOK) BBRI_P_RFSOK, SUM (BBNI_L_RFSOK) BBNI_L_RFSOK, SUM (BBNI_P_RFSOK) BBNI_P_RFSOK, SUM (BBCA_L_RFSOK) BBCA_L_RFSOK, SUM (BBCA_P_RFSOK) BBCA_P_RFSOK, SUM (MDRI_L_RFSNOK) MDRI_L_RFSNOK, SUM (MDRI_P_RFSNOK) MDRI_P_RFSNOK, SUM (BBRI_L_RFSNOK) BBRI_L_RFSNOK, SUM (BBRI_P_RFSNOK) BBRI_P_RFSNOK, SUM (BBNI_L_RFSNOK) BBNI_L_RFSNOK, SUM (BBNI_P_RFSNOK) BBNI_P_RFSNOK, SUM (BBCA_L_RFSNOK) BBCA_L_RFSNOK, SUM (BBCA_P_RFSNOK) BBCA_P_RFSNOK, LISTAGG (list_reject, ', ') WITHIN GROUP (ORDER BY TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT ASC) list_reject FROM (SELECT * FROM ( SELECT TGL_REPORT, KODE_BANK, KODE_CABANG, KODE_GERBANG, TRIM (nama_gerbang) NAMA_GERBANG, SHIFT, SUM (LALIN_ATT4) LALIN_ATT4, SUM (PENDAPATAN_ATT4) PENDAPATAN_ATT4, SUM (LALIN_JURNAL) LALIN_JURNAL, SUM (PENDAPATAN_JURNAL) PENDAPATAN_JURNAL, SUM (LALIN_REJECT) LALIN_REJECT, SUM (PENDAPATAN_REJECT) PENDAPATAN_REJECT, SUM (LALIN_JURNAL_REJECT) LALIN_JURNAL_REJECT, SUM (PENDAPATAN_JURNAL_REJECT) PENDAPATAN_JURNAL_REJECT, SUM (LALIN_FS) LALIN_FS, SUM (PENDAPATAN_FS) PENDAPATAN_FS, SUM (LALIN_RFS_OK) LALIN_RFS_OK, SUM (PENDAPATAN_RFS_OK) PENDAPATAN_RFS_OK, SUM (LALIN_RFS_NOK) LALIN_RFS_NOK, SUM (PENDAPATAN_RFS_NOK) PENDAPATAN_RFS_NOK, LISTAGG (list_reject, ', ') WITHIN GROUP (ORDER BY TGL_REPORT, KODE_BANK, KODE_CABANG, KODE_GERBANG, SHIFT ASC) list_reject FROM ( SELECT TGL_REPORT, pkg_001commons.f_007getbankid (kode_bank) KODE_BANK, KODE_CABANG, KODE_GERBANG, PKG_001COMMONS.f_013getgerbangname (KODE_CABANG, KODE_GERBANG, kode_bank) NAMA_GERBANG, SHIFT, LALIN_ATT4, PENDAPATAN_ATT4, LALIN_JURNAL, PENDAPATAN_JURNAL, LALIN_REJECT, PENDAPATAN_REJECT, LALIN_JURNAL_REJECT, PENDAPATAN_JURNAL_REJECT, LALIN_FS, PENDAPATAN_FS, LALIN_RFS_OK, PENDAPATAN_RFS_OK, LALIN_RFS_NOK, PENDAPATAN_RFS_NOK, CASE WHEN list_reject IS NULL THEN list_reject ELSE KODE_BANK || ' ( ' || list_reject || ' ) ' END list_reject FROM ( SELECT COALESCE (a.tgl_report, b.tgl_report) tgl_report, COALESCE (a.kode_bank, b.kode_bank) kode_bank, COALESCE (a.kode_cabang, b.kode_cabang) kode_cabang, COALESCE (a.kode_gerbang, b.kode_gerbang) kode_gerbang, COALESCE (a.shift, b.shift) shift, NVL (a.lalin_att4, 0) lalin_att4, NVL (a.pendapatan_att4, 0) pendapatan_att4, NVL (a.lalin_jurnal, 0) lalin_jurnal, NVL (a.pendapatan_jurnal, 0) pendapatan_jurnal, NVL (b.jml_transaksi, 0) lalin_reject, NVL (b.total_amount, 0) pendapatan_reject, CASE WHEN NVL (b.jml_transaksi, 0) = 0 THEN 0 ELSE NVL (a.lalin_jurnal, 0) + NVL (b.jml_transaksi, 0) END lalin_jurnal_reject, CASE WHEN NVL (b.total_amount, 0) = 0 THEN 0 ELSE NVL (a.pendapatan_jurnal, 0) + NVL (b.total_amount, 0) END pendapatan_jurnal_reject, NVL (a.lalin_fs, 0) lalin_fs, NVL (a.pendapatan_fs, 0) pendapatan_fs, NVL (a.lalin_rfs_ok, 0) lalin_rfs_ok, NVL (a.pendapatan_rfs_ok, 0) pendapatan_rfs_ok, NVL (a.lalin_rfs_nok, 0) lalin_rfs_nok, NVL (a.pendapatan_rfs_nok, 0) pendapatan_rfs_nok, list_reject FROM ( SELECT TO_NUMBER (tgl_report) tgl_report, kode_bank, TO_NUMBER (kode_cabang) kode_cabang, TO_NUMBER (kode_gerbang) kode_gerbang, TO_NUMBER (shift) shift, SUM (lalin_att4) lalin_att4, SUM (pendapatan_att4) pendapatan_att4, SUM (lalin_jurnal) lalin_jurnal, SUM (pendapatan_jurnal) pendapatan_jurnal, SUM (lalin_fs) lalin_fs, SUM (pendapatan_fs) pendapatan_fs, SUM (lalin_rfs_ok) lalin_rfs_ok, SUM (pendapatan_rfs_ok) pendapatan_rfs_ok, SUM (lalin_rfs_nok) lalin_rfs_nok, SUM (pendapatan_rfs_nok) pendapatan_rfs_nok FROM rekap_fs_rfs_rk WHERE tgl_report = '".$request['tgl_report']."' GROUP BY TO_NUMBER (tgl_report), kode_bank, TO_NUMBER (kode_cabang), TO_NUMBER (kode_gerbang), TO_NUMBER (shift) ORDER BY TO_NUMBER (tgl_report), kode_bank, TO_NUMBER (kode_cabang), TO_NUMBER (kode_gerbang), TO_NUMBER (shift) ) a FULL OUTER JOIN (SELECT tgl_report, kode_bank, kode_cabang, kode_gerbang, shift, SUM (jml_transaksi_tot) jml_transaksi_tot, SUM (total_amount_tot) total_amount_tot, SUM (jml_transaksi_dup) jml_transaksi_dup, SUM (total_amount_dup) total_amount_dup, SUM (jml_transaksi) jml_transaksi, SUM (total_amount) total_amount, LISTAGG (error_desc, ', ') WITHIN GROUP (ORDER BY tgl_report, kode_bank, kode_cabang, kode_gerbang, shift, error_desc ASC) list_reject FROM (SELECT TO_NUMBER (tgl_report) tgl_report, kode_bank, TO_NUMBER (kode_cabang) kode_cabang, TO_NUMBER (kode_gerbang) kode_gerbang, TO_NUMBER (shift) shift, error_desc, SUM (jml_transaksi_tot) jml_transaksi_tot, SUM (total_amount_dup_tot) total_amount_tot, SUM (jml_transaksi_dup) jml_transaksi_dup, SUM (total_amount_dup) total_amount_dup, SUM (jml_transaksi) jml_transaksi, SUM (total_amount) total_amount FROM (SELECT tgl_report, kode_bank, kode_cabang, kode_gerbang, shift, error_desc, jml_transaksi_dup jml_transaksi_tot, total_amount_dup total_amount_dup_tot, jml_transaksi, total_amount, jml_transaksi_dup - jml_transaksi jml_transaksi_dup, total_amount_dup - total_amount total_amount_dup FROM (SELECT DISTINCT tgl_report, pkg_001commons.f_008getbankname (metoda) kode_bank, idcabang kode_cabang, idgerbang kode_gerbang, shift, signature, error_id || ' - ' || pkg_001commons.f_011_geterrorname (error_id) error_desc, COUNT (DISTINCT NVL (signature, rupiah)) OVER (PARTITION BY tgl_report, metoda, idcabang, idgerbang, shift, signature) AS jml_transaksi, COUNT (NVL (signature, rupiah)) OVER (PARTITION BY tgl_report, metoda, idcabang, idgerbang, shift, signature) jml_transaksi_dup, SUM (DISTINCT rupiah) OVER (PARTITION BY tgl_report, metoda, idcabang, idgerbang, shift, signature) total_amount, SUM (rupiah) OVER (PARTITION BY tgl_report, metoda, idcabang, idgerbang, shift, signature) total_amount_dup FROM t_reject_lalin WHERE NVL (status_reject, 0) = 0 AND  tgl_report = '".$request['tgl_report']."' AND  shift = '1')) GROUP BY tgl_report, kode_bank, kode_cabang, kode_gerbang, shift, error_desc) GROUP BY tgl_report, kode_bank, kode_cabang, kode_gerbang, shift ORDER BY tgl_report, kode_bank, kode_cabang, kode_gerbang, shift) b ON a.tgl_report = b.tgl_report AND a.kode_bank = b.kode_bank AND a.kode_cabang = b.kode_cabang AND a.kode_gerbang = b.kode_gerbang AND a.shift = b.shift ORDER BY a.tgl_report, a.kode_cabang, a.kode_gerbang, a.shift, pkg_001commons.f_007getbankid (a.kode_bank) ) WHERE TGL_REPORT = '".$request['tgl_report']."' AND  SHIFT = ".$request['shift']." ) GROUP BY TGL_REPORT, KODE_BANK, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT ) PIVOT (SUM (LALIN_ATT4) AS L_AT4, SUM (PENDAPATAN_ATT4) P_AT4, SUM (LALIN_JURNAL) AS L_JN, SUM (PENDAPATAN_JURNAL) AS P_JN, SUM (LALIN_REJECT) AS L_RJ, SUM (PENDAPATAN_REJECT) AS P_RJ, SUM (LALIN_JURNAL_REJECT) AS L_JNRJ, SUM (PENDAPATAN_JURNAL_REJECT) AS P_JNRJ, SUM (LALIN_FS) AS L_FS, SUM (PENDAPATAN_FS) AS P_FS, SUM (LALIN_RFS_OK) AS L_RFSOK, SUM (PENDAPATAN_RFS_OK) AS P_RFSOK, SUM (LALIN_RFS_NOK) AS L_RFSNOK, SUM (PENDAPATAN_RFS_NOK) AS P_RFSNOK FOR KODE_BANK IN (3 AS MDRI, 20 AS BBRI, 21 BBNI, 23 AS BBCA)) ORDER BY TGL_REPORT, KODE_CABANG, SHIFT, KODE_GERBANG) GROUP BY TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT) ORDER BY TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT";

            $fileName = "Rekap_" . $request['nama_cabang'] . "_tglreport" . $request['tgl_report']."_shift".$request['shift'];
            $fileCaption = "Data Rekap Cabang " . strtoupper($request['nama_cabang']) . "\nTanggal Report " . $request['tgl_report'] . "\nShift " . $request['shift'];
            $fileNameEx = $fileName . ".xlsx";

            try
            {
                DB::connection('oracle')->getPdo();
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }

            try
            {
                if ($response = DB::connection('oracle')->select($query))
                {
                    if ((new RekapExport($response))->store($fileNameEx))
                    {
//                        $filePath = storage_path('app');
//                        exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                        $fileUrl = url('download/' . $fileName);

                        return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                    }
                    else
                    {
                        return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                    }
                }
                else
                {
                    return $this->lib->response(FALSE, $fileCaption . "\nis empty", FALSE, FALSE, 200);
                }
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }
        }
        else
        {
            return $this->lib->response(FALSE, "Cant find cabang " . $request['nama_cabang'], FALSE, FALSE, 200);
        }
    }

    public function rekapAllShift(Request $request)
    {
        if (empty($request['nama_cabang']))
        {
            return $this->lib->response(TRUE, "nama cabang cant be empty", FALSE, FALSE, 200);
        }

        if ($dataCabang = Cabang::all()->firstWhere('nama_cabang', strtoupper($request['nama_cabang'])))
        {
            Config::set('database.connections.oracle.host', $dataCabang['host']);
            Config::set('database.connections.oracle.port', $dataCabang['port']);
            Config::set('database.connections.oracle.username', $dataCabang['username']);
            Config::set('database.connections.oracle.password', $dataCabang['password']);
            Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

            $query = "SELECT TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT, MDRI_L_AT4, MDRI_P_AT4, BBRI_L_AT4, BBRI_P_AT4, BBNI_L_AT4, BBNI_P_AT4, BBCA_L_AT4, BBCA_P_AT4, MDRI_L_AT4 + BBRI_L_AT4 + BBNI_L_AT4 + BBCA_L_AT4 TOT_L_AT4, MDRI_P_AT4 + BBRI_P_AT4 + BBNI_P_AT4 + BBCA_P_AT4 TOT_P_AT4, MDRI_L_JN, MDRI_P_JN, BBRI_L_JN, BBRI_P_JN, BBNI_L_JN, BBNI_P_JN, BBCA_L_JN, BBCA_P_JN, MDRI_L_JN + BBRI_L_JN + BBNI_L_JN + BBCA_L_JN TOT_L_JN, MDRI_P_JN + BBRI_P_JN + BBNI_P_JN + BBCA_P_JN TOT_P_JN, MDRI_L_RJ, MDRI_P_RJ, BBRI_L_RJ, BBRI_P_RJ, BBNI_L_RJ, BBNI_P_RJ, BBCA_L_RJ, BBCA_P_RJ, MDRI_L_RJ + BBRI_L_RJ + BBNI_L_RJ + BBCA_L_RJ TOT_L_RJ, MDRI_P_RJ + BBRI_P_RJ + BBNI_P_RJ + BBCA_P_RJ TOT_P_RJ, MDRI_L_JNRJ, MDRI_P_JNRJ, BBRI_L_JNRJ, BBRI_P_JNRJ, BBNI_L_JNRJ, BBNI_P_JNRJ, BBCA_L_JNRJ, BBCA_P_JNRJ, MDRI_L_JNRJ + BBRI_L_JNRJ + BBNI_L_JNRJ + BBCA_L_JNRJ TOT_L_JNRJ, MDRI_P_JNRJ + BBRI_P_JNRJ + BBNI_P_JNRJ + BBCA_P_JNRJ TOT_P_JNRJ, MDRI_L_FS, MDRI_P_FS, BBRI_L_FS, BBRI_P_FS, BBNI_L_FS, BBNI_P_FS, BBCA_L_FS, BBCA_P_FS, MDRI_L_FS + BBRI_L_FS + BBNI_L_FS + BBCA_L_FS TOT_L_FS, MDRI_P_FS + BBRI_P_FS + BBNI_P_FS + BBCA_P_FS TOT_P_FS, MDRI_L_RFSOK, MDRI_P_RFSOK, BBRI_L_RFSOK, BBRI_P_RFSOK, BBNI_L_RFSOK, BBNI_P_RFSOK, BBCA_L_RFSOK, BBCA_P_RFSOK, MDRI_L_RFSOK + BBRI_L_RFSOK + BBNI_L_RFSOK + BBCA_L_RFSOK TOT_L_RFSOK, MDRI_P_RFSOK + BBRI_P_RFSOK + BBNI_P_RFSOK + BBCA_P_RFSOK TOT_P_RFSOK, MDRI_L_RFSNOK, MDRI_P_RFSNOK, BBRI_L_RFSNOK, BBRI_P_RFSNOK, BBNI_L_RFSNOK, BBNI_P_RFSNOK, BBCA_L_RFSNOK, BBCA_P_RFSNOK, MDRI_L_RFSNOK + BBRI_L_RFSNOK + BBNI_L_RFSNOK + BBCA_L_RFSNOK TOT_L_RFSNOK, MDRI_P_RFSNOK + BBRI_P_RFSNOK + BBNI_P_RFSNOK + BBCA_P_RFSNOK TOT_P_RFSNOK, list_reject FROM (SELECT TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT, SUM (MDRI_L_AT4) MDRI_L_AT4, SUM (MDRI_P_AT4) MDRI_P_AT4, SUM (BBRI_L_AT4) BBRI_L_AT4, SUM (BBRI_P_AT4) BBRI_P_AT4, SUM (BBNI_L_AT4) BBNI_L_AT4, SUM (BBNI_P_AT4) BBNI_P_AT4, SUM (BBCA_L_AT4) BBCA_L_AT4, SUM (BBCA_P_AT4) BBCA_P_AT4, SUM (MDRI_L_JN) MDRI_L_JN, SUM (MDRI_P_JN) MDRI_P_JN, SUM (BBRI_L_JN) BBRI_L_JN, SUM (BBRI_P_JN) BBRI_P_JN, SUM (BBNI_L_JN) BBNI_L_JN, SUM (BBNI_P_JN) BBNI_P_JN, SUM (BBCA_L_JN) BBCA_L_JN, SUM (BBCA_P_JN) BBCA_P_JN, SUM (MDRI_L_RJ) MDRI_L_RJ, SUM (MDRI_P_RJ) MDRI_P_RJ, SUM (BBRI_L_RJ) BBRI_L_RJ, SUM (BBRI_P_RJ) BBRI_P_RJ, SUM (BBNI_L_RJ) BBNI_L_RJ, SUM (BBNI_P_RJ) BBNI_P_RJ, SUM (BBCA_L_RJ) BBCA_L_RJ, SUM (BBCA_P_RJ) BBCA_P_RJ, SUM (MDRI_L_JNRJ) MDRI_L_JNRJ, SUM (MDRI_P_JNRJ) MDRI_P_JNRJ, SUM (BBRI_L_JNRJ) BBRI_L_JNRJ, SUM (BBRI_P_JNRJ) BBRI_P_JNRJ, SUM (BBNI_L_JNRJ) BBNI_L_JNRJ, SUM (BBNI_P_JNRJ) BBNI_P_JNRJ, SUM (BBCA_L_JNRJ) BBCA_L_JNRJ, SUM (BBCA_P_JNRJ) BBCA_P_JNRJ, SUM (MDRI_L_FS) MDRI_L_FS, SUM (MDRI_P_FS) MDRI_P_FS, SUM (BBRI_L_FS) BBRI_L_FS, SUM (BBRI_P_FS) BBRI_P_FS, SUM (BBNI_L_FS) BBNI_L_FS, SUM (BBNI_P_FS) BBNI_P_FS, SUM (BBCA_L_FS) BBCA_L_FS, SUM (BBCA_P_FS) BBCA_P_FS, SUM (MDRI_L_RFSOK) MDRI_L_RFSOK, SUM (MDRI_P_RFSOK) MDRI_P_RFSOK, SUM (BBRI_L_RFSOK) BBRI_L_RFSOK, SUM (BBRI_P_RFSOK) BBRI_P_RFSOK, SUM (BBNI_L_RFSOK) BBNI_L_RFSOK, SUM (BBNI_P_RFSOK) BBNI_P_RFSOK, SUM (BBCA_L_RFSOK) BBCA_L_RFSOK, SUM (BBCA_P_RFSOK) BBCA_P_RFSOK, SUM (MDRI_L_RFSNOK) MDRI_L_RFSNOK, SUM (MDRI_P_RFSNOK) MDRI_P_RFSNOK, SUM (BBRI_L_RFSNOK) BBRI_L_RFSNOK, SUM (BBRI_P_RFSNOK) BBRI_P_RFSNOK, SUM (BBNI_L_RFSNOK) BBNI_L_RFSNOK, SUM (BBNI_P_RFSNOK) BBNI_P_RFSNOK, SUM (BBCA_L_RFSNOK) BBCA_L_RFSNOK, SUM (BBCA_P_RFSNOK) BBCA_P_RFSNOK, LISTAGG (list_reject, ', ') WITHIN GROUP (ORDER BY SHIFT, TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG) list_reject FROM (SELECT * FROM ( SELECT TGL_REPORT, KODE_BANK, KODE_CABANG, KODE_GERBANG, TRIM (nama_gerbang) NAMA_GERBANG, SHIFT, SUM (LALIN_ATT4) LALIN_ATT4, SUM (PENDAPATAN_ATT4) PENDAPATAN_ATT4, SUM (LALIN_JURNAL) LALIN_JURNAL, SUM (PENDAPATAN_JURNAL) PENDAPATAN_JURNAL, SUM (LALIN_REJECT) LALIN_REJECT, SUM (PENDAPATAN_REJECT) PENDAPATAN_REJECT, SUM (LALIN_JURNAL_REJECT) LALIN_JURNAL_REJECT, SUM (PENDAPATAN_JURNAL_REJECT) PENDAPATAN_JURNAL_REJECT, SUM (LALIN_FS) LALIN_FS, SUM (PENDAPATAN_FS) PENDAPATAN_FS, SUM (LALIN_RFS_OK) LALIN_RFS_OK, SUM (PENDAPATAN_RFS_OK) PENDAPATAN_RFS_OK, SUM (LALIN_RFS_NOK) LALIN_RFS_NOK, SUM (PENDAPATAN_RFS_NOK) PENDAPATAN_RFS_NOK, LISTAGG (list_reject, ', ') WITHIN GROUP (ORDER BY SHIFT, TGL_REPORT, KODE_BANK, KODE_CABANG, KODE_GERBANG) list_reject FROM ( SELECT TGL_REPORT, pkg_001commons.f_007getbankid (kode_bank) KODE_BANK, KODE_CABANG, KODE_GERBANG, PKG_001COMMONS.f_013getgerbangname (KODE_CABANG, KODE_GERBANG, kode_bank) NAMA_GERBANG, SHIFT, LALIN_ATT4, PENDAPATAN_ATT4, LALIN_JURNAL, PENDAPATAN_JURNAL, LALIN_REJECT, PENDAPATAN_REJECT, LALIN_JURNAL_REJECT, PENDAPATAN_JURNAL_REJECT, LALIN_FS, PENDAPATAN_FS, LALIN_RFS_OK, PENDAPATAN_RFS_OK, LALIN_RFS_NOK, PENDAPATAN_RFS_NOK, CASE WHEN list_reject IS NULL THEN list_reject ELSE KODE_BANK || ' ( ' || list_reject || ' ) ' END list_reject FROM ( SELECT COALESCE (a.tgl_report, b.tgl_report) tgl_report, COALESCE (a.kode_bank, b.kode_bank) kode_bank, COALESCE (a.kode_cabang, b.kode_cabang) kode_cabang, COALESCE (a.kode_gerbang, b.kode_gerbang) kode_gerbang, COALESCE (a.shift, b.shift) shift, NVL (a.lalin_att4, 0) lalin_att4, NVL (a.pendapatan_att4, 0) pendapatan_att4, NVL (a.lalin_jurnal, 0) lalin_jurnal, NVL (a.pendapatan_jurnal, 0) pendapatan_jurnal, NVL (b.jml_transaksi, 0) lalin_reject, NVL (b.total_amount, 0) pendapatan_reject, CASE WHEN NVL (b.jml_transaksi, 0) = 0 THEN 0 ELSE NVL (a.lalin_jurnal, 0) + NVL (b.jml_transaksi, 0) END lalin_jurnal_reject, CASE WHEN NVL (b.total_amount, 0) = 0 THEN 0 ELSE NVL (a.pendapatan_jurnal, 0) + NVL (b.total_amount, 0) END pendapatan_jurnal_reject, NVL (a.lalin_fs, 0) lalin_fs, NVL (a.pendapatan_fs, 0) pendapatan_fs, NVL (a.lalin_rfs_ok, 0) lalin_rfs_ok, NVL (a.pendapatan_rfs_ok, 0) pendapatan_rfs_ok, NVL (a.lalin_rfs_nok, 0) lalin_rfs_nok, NVL (a.pendapatan_rfs_nok, 0) pendapatan_rfs_nok, list_reject FROM ( SELECT TO_NUMBER (tgl_report) tgl_report, kode_bank, TO_NUMBER (kode_cabang) kode_cabang, TO_NUMBER (kode_gerbang) kode_gerbang, TO_NUMBER (shift) shift, SUM (lalin_att4) lalin_att4, SUM (pendapatan_att4) pendapatan_att4, SUM (lalin_jurnal) lalin_jurnal, SUM (pendapatan_jurnal) pendapatan_jurnal, SUM (lalin_fs) lalin_fs, SUM (pendapatan_fs) pendapatan_fs, SUM (lalin_rfs_ok) lalin_rfs_ok, SUM (pendapatan_rfs_ok) pendapatan_rfs_ok, SUM (lalin_rfs_nok) lalin_rfs_nok, SUM (pendapatan_rfs_nok) pendapatan_rfs_nok FROM rekap_fs_rfs_rk WHERE tgl_report = to_char(sysdate,'yyyymmdd')-1 GROUP BY TO_NUMBER (tgl_report), kode_bank, TO_NUMBER (kode_cabang), TO_NUMBER (kode_gerbang), TO_NUMBER (shift) ORDER BY TO_NUMBER (tgl_report), kode_bank, TO_NUMBER (kode_cabang), TO_NUMBER (kode_gerbang) ) a FULL OUTER JOIN (SELECT tgl_report, kode_bank, kode_cabang, kode_gerbang, shift, SUM (jml_transaksi_tot) jml_transaksi_tot, SUM (total_amount_tot) total_amount_tot, SUM (jml_transaksi_dup) jml_transaksi_dup, SUM (total_amount_dup) total_amount_dup, SUM (jml_transaksi) jml_transaksi, SUM (total_amount) total_amount, LISTAGG (error_desc, ', ') WITHIN GROUP (ORDER BY shift, tgl_report, kode_bank, kode_cabang, kode_gerbang, error_desc ASC) list_reject FROM (SELECT TO_NUMBER (tgl_report) tgl_report, kode_bank, TO_NUMBER (kode_cabang) kode_cabang, TO_NUMBER (kode_gerbang) kode_gerbang, TO_NUMBER (shift) shift, error_desc, SUM (jml_transaksi_tot) jml_transaksi_tot, SUM (total_amount_dup_tot) total_amount_tot, SUM (jml_transaksi_dup) jml_transaksi_dup, SUM (total_amount_dup) total_amount_dup, SUM (jml_transaksi) jml_transaksi, SUM (total_amount) total_amount FROM (SELECT tgl_report, kode_bank, kode_cabang, kode_gerbang, shift, error_desc, jml_transaksi_dup jml_transaksi_tot, total_amount_dup total_amount_dup_tot, jml_transaksi, total_amount, jml_transaksi_dup - jml_transaksi jml_transaksi_dup, total_amount_dup - total_amount total_amount_dup FROM (SELECT DISTINCT tgl_report, pkg_001commons.f_008getbankname (metoda) kode_bank, idcabang kode_cabang, idgerbang kode_gerbang, shift, signature, error_id || ' - ' || pkg_001commons.f_011_geterrorname (error_id) error_desc, COUNT (DISTINCT NVL (signature, rupiah)) OVER (PARTITION BY tgl_report, metoda, idcabang, idgerbang, shift, signature) AS jml_transaksi, COUNT (NVL (signature, rupiah)) OVER (PARTITION BY tgl_report, metoda, idcabang, idgerbang, shift, signature) jml_transaksi_dup, SUM (DISTINCT rupiah) OVER (PARTITION BY tgl_report, metoda, idcabang, idgerbang, shift, signature) total_amount, SUM (rupiah) OVER (PARTITION BY tgl_report, metoda, idcabang, idgerbang, shift, signature) total_amount_dup FROM t_reject_lalin WHERE NVL (status_reject, 0) = 0 AND   tgl_report = to_char(sysdate,'yyyymmdd')-1)) GROUP BY tgl_report, kode_bank, kode_cabang, kode_gerbang, shift, error_desc) GROUP BY tgl_report, kode_bank, kode_cabang, kode_gerbang, shift ORDER BY  shift, tgl_report, kode_bank, kode_cabang, kode_gerbang) b ON a.tgl_report = b.tgl_report AND a.kode_bank = b.kode_bank AND a.kode_cabang = b.kode_cabang AND a.kode_gerbang = b.kode_gerbang AND a.shift = b.shift ORDER BY a.tgl_report, a.kode_cabang, a.kode_gerbang, pkg_001commons.f_007getbankid (a.kode_bank) ) WHERE TGL_REPORT = to_char(sysdate,'yyyymmdd')-1) GROUP BY TGL_REPORT, KODE_BANK, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT ) PIVOT (SUM (LALIN_ATT4) AS L_AT4, SUM (PENDAPATAN_ATT4) P_AT4, SUM (LALIN_JURNAL) AS L_JN, SUM (PENDAPATAN_JURNAL) AS P_JN, SUM (LALIN_REJECT) AS L_RJ, SUM (PENDAPATAN_REJECT) AS P_RJ, SUM (LALIN_JURNAL_REJECT) AS L_JNRJ, SUM (PENDAPATAN_JURNAL_REJECT) AS P_JNRJ, SUM (LALIN_FS) AS L_FS, SUM (PENDAPATAN_FS) AS P_FS, SUM (LALIN_RFS_OK) AS L_RFSOK, SUM (PENDAPATAN_RFS_OK) AS P_RFSOK, SUM (LALIN_RFS_NOK) AS L_RFSNOK, SUM (PENDAPATAN_RFS_NOK) AS P_RFSNOK FOR KODE_BANK IN (3 AS MDRI, 20 AS BBRI, 21 BBNI, 23 AS BBCA)) ORDER BY TGL_REPORT, KODE_CABANG, SHIFT, KODE_GERBANG) GROUP BY TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG, SHIFT) ORDER BY  SHIFT, TGL_REPORT, KODE_CABANG, KODE_GERBANG, NAMA_GERBANG";

            $fileName = "RekapAllShift_" . $request['nama_cabang'];
            $fileCaption = "Data Rekap All Shift Cabang " . strtoupper($request['nama_cabang']);
            $fileNameEx = $fileName . ".xlsx";

            try
            {
                DB::connection('oracle')->getPdo();
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }

            try
            {
                if ($response = DB::connection('oracle')->select($query))
                {
                    if ((new RekapExport($response))->store($fileNameEx))
                    {
//                        $filePath = storage_path('app');
//                        exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                        $fileUrl = url('download/' . $fileName);

                        return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                    }
                    else
                    {
                        return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                    }
                }
                else
                {
                    return $this->lib->response(FALSE, $fileCaption . "\nis empty", FALSE, FALSE, 200);
                }
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }
        }
        else
        {
            return $this->lib->response(FALSE, "Cant find cabang " . $request['nama_cabang'], FALSE, FALSE, 200);
        }
    }

    public function cekTableSpace(Request $request)
    {
        if (empty($request['nama_cabang']))
        {
            return $this->lib->response(TRUE, "Nama cabang cant be empty", FALSE, FALSE, 200);
        }

        if ($dataCabang = Cabang::all()->firstWhere('nama_cabang', strtoupper($request['nama_cabang'])))
        {
            Config::set('database.connections.oracle.host', $dataCabang['host']);
            Config::set('database.connections.oracle.port', $dataCabang['port']);
            Config::set('database.connections.oracle.username', $dataCabang['username']);
            Config::set('database.connections.oracle.password', $dataCabang['password']);
            Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

            $query = "SELECT df.tablespace_name tablespace_name, MAX (df.autoextensible) auto_ext, ROUND (df.maxbytes / (1024 * 1024), 2) max_ts_size,
                   ROUND ((df.bytes - SUM (fs.bytes)) / (df.maxbytes) * 100, 2) max_ts_pct_used, ROUND (df.bytes / (1024 * 1024), 2) curr_ts_size, ROUND ((df.bytes - SUM (fs.bytes)) / (1024 * 1024), 2) used_ts_size,
                   ROUND ((df.bytes - SUM (fs.bytes)) * 100 / df.bytes, 2) ts_pct_used, ROUND (SUM (fs.bytes) / (1024 * 1024), 2) free_ts_size, NVL (ROUND (SUM (fs.bytes) * 100 / df.bytes), 2) ts_pct_free
            FROM dba_free_space fs,
                 (
                     SELECT tablespace_name, SUM (bytes) bytes, SUM (DECODE (maxbytes, 0, bytes, maxbytes)) maxbytes,
                            MAX (autoextensible) autoextensible
                     FROM dba_data_files
                     GROUP BY tablespace_name
                 ) df
            WHERE fs.tablespace_name(+) = df.tablespace_name
            GROUP BY df.tablespace_name, df.bytes, df.maxbytes
            UNION ALL
            SELECT df.tablespace_name tablespace_name, MAX (df.autoextensible) auto_ext, ROUND (df.maxbytes / (1024 * 1024), 2) max_ts_size,
                   ROUND ((df.bytes - SUM (fs.bytes)) / (df.maxbytes) * 100, 2) max_ts_pct_used, ROUND (df.bytes / (1024 * 1024), 2) curr_ts_size, ROUND ((df.bytes - SUM (fs.bytes)) / (1024 * 1024), 2) used_ts_size,
                   ROUND ((df.bytes - SUM (fs.bytes)) * 100 / df.bytes, 2) ts_pct_used, ROUND (SUM (fs.bytes) / (1024 * 1024), 2) free_ts_size, NVL (ROUND (SUM (fs.bytes) * 100 / df.bytes), 2) ts_pct_free
            FROM (
                     SELECT tablespace_name, bytes_used bytes
                     FROM V$"."temp_space_header
                     GROUP BY tablespace_name, bytes_free, bytes_used
                 ) fs,
                 (
                     SELECT tablespace_name, SUM (bytes) bytes, SUM (DECODE (maxbytes, 0, bytes, maxbytes)) maxbytes,
                            MAX (autoextensible) autoextensible
                     FROM dba_temp_files
                     GROUP BY tablespace_name
                 ) df
            WHERE fs.tablespace_name(+) = df.tablespace_name
            GROUP BY df.tablespace_name, df.bytes, df.maxbytes
            ORDER BY 1";
            $fileName    = "TableSpace_" . $request['nama_cabang'];
            $fileCaption = "Table Space Cabang " . strtoupper($request['nama_cabang']);
            $fileNameEx  = $fileName . ".xlsx";

            try
            {
                DB::connection('oracle')->getPdo();
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }

            try
            {
                if ($response = DB::connection('oracle')->select($query))
                {
                    if ((new TableSpaceExport($response))->store($fileNameEx))
                    {
                        //                        $filePath = storage_path('app');
//                        exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                        $fileUrl = url('download/' . $fileName);

                        return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                    }
                    else
                    {
                        return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                    }
                }
                else
                {
                    return $this->lib->response(FALSE, $fileCaption . "\nis empty", FALSE, FALSE, 200);
                }
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }
        }
        else
        {
            return $this->lib->response(FALSE, "Cant find cabang " . $request['nama_cabang'], FALSE, FALSE, 200);
        }
    }

    public function cekTableSpaceStatus(Request $request)
    {
        if (empty($request['nama_cabang']))
        {
            return $this->lib->response(TRUE, "Nama cabang cant be empty", FALSE, FALSE, 200);
        }

        if ($dataCabang = Cabang::all()->firstWhere('nama_cabang', strtoupper($request['nama_cabang'])))
        {
            Config::set('database.connections.oracle.host', $dataCabang['host']);
            Config::set('database.connections.oracle.port', $dataCabang['port']);
            Config::set('database.connections.oracle.username', $dataCabang['username']);
            Config::set('database.connections.oracle.password', $dataCabang['password']);
            Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

            $query = "select * from (
            SELECT df.tablespace_name tablespace_name, MAX (df.autoextensible) auto_ext, ROUND (df.maxbytes / (1024 * 1024), 2) max_ts_size,
                   ROUND ((df.bytes - SUM (fs.bytes)) / (df.maxbytes) * 100, 2) max_ts_pct_used, ROUND (df.bytes / (1024 * 1024), 2) curr_ts_size, ROUND ((df.bytes - SUM (fs.bytes)) / (1024 * 1024), 2) used_ts_size,
                   ROUND ((df.bytes - SUM (fs.bytes)) * 100 / df.bytes, 2) ts_pct_used, ROUND (SUM (fs.bytes) / (1024 * 1024), 2) free_ts_size, NVL (ROUND (SUM (fs.bytes) * 100 / df.bytes), 2) ts_pct_free
            FROM dba_free_space fs,
                 (
                     SELECT tablespace_name, SUM (bytes) bytes, SUM (DECODE (maxbytes, 0, bytes, maxbytes)) maxbytes,
                            MAX (autoextensible) autoextensible
                     FROM dba_data_files
                     GROUP BY tablespace_name
                 ) df
            WHERE fs.tablespace_name(+) = df.tablespace_name
            GROUP BY df.tablespace_name, df.bytes, df.maxbytes
            UNION ALL
            SELECT df.tablespace_name tablespace_name, MAX (df.autoextensible) auto_ext, ROUND (df.maxbytes / (1024 * 1024), 2) max_ts_size,
                   ROUND ((df.bytes - SUM (fs.bytes)) / (df.maxbytes) * 100, 2) max_ts_pct_used, ROUND (df.bytes / (1024 * 1024), 2) curr_ts_size, ROUND ((df.bytes - SUM (fs.bytes)) / (1024 * 1024), 2) used_ts_size,
                   ROUND ((df.bytes - SUM (fs.bytes)) * 100 / df.bytes, 2) ts_pct_used, ROUND (SUM (fs.bytes) / (1024 * 1024), 2) free_ts_size, NVL (ROUND (SUM (fs.bytes) * 100 / df.bytes), 2) ts_pct_free
            FROM (
                     SELECT tablespace_name, bytes_used bytes
                     FROM V$" . "temp_space_header
                     GROUP BY tablespace_name, bytes_free, bytes_used
                 ) fs,
                 (
                     SELECT tablespace_name, SUM (bytes) bytes, SUM (DECODE (maxbytes, 0, bytes, maxbytes)) maxbytes,
                            MAX (autoextensible) autoextensible
                     FROM dba_temp_files
                     GROUP BY tablespace_name
                 ) df
            WHERE fs.tablespace_name(+) = df.tablespace_name
            GROUP BY df.tablespace_name, df.bytes, df.maxbytes )
            where max_ts_pct_used > 80
            order by 1";
            $fileName = "TableSpaceStatus_" . $request['nama_cabang'];
            $fileCaption = "Table Space Status > 80% Cabang " . strtoupper($request['nama_cabang']);

            $fileNameEx = $fileName . ".xlsx";

            try
            {
                DB::connection('oracle')->getPdo();
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }

            try
            {
                if ($response = DB::connection('oracle')->select($query))
                {
                    if ((new TableSpaceExport($response))->store($fileNameEx))
                    {
//                        $filePath = storage_path('app');
//                        exec("cd " .$filePath . ";zip -r " . $fileName . ".zip " . $fileNameEx);
//
//                        $fileUrl = url('download/' . $fileName);

                        return $this->lib->response(TRUE, $fileCaption . "\n" . "http://telebot.jmto.co.id/download/" . $fileName, FALSE, FALSE, 200);
                    }
                    else
                    {
                        return $this->lib->response(FALSE, "Generate data failed", FALSE, FALSE, 200);
                    }
                }
                else
                {
                    return $this->lib->response(FALSE, "There are no tablespaces with more than 80% usage on cabang " . strtoupper($request['nama_cabang']), FALSE, FALSE, 200);
                }
            }
            catch (\Exception $e)
            {
                return $this->lib->response(FALSE, $e->getMessage(), FALSE, FALSE, 200);
            }
        }
        else
        {
            return $this->lib->response(FALSE, "Cant find cabang " . $request['nama_cabang'], FALSE, FALSE, 200);
        }
    }
}

