<?php namespace App\Http\Controllers;

use App\Cabang;
use App\Exports\TableSpaceExport;
use App\Lib\Lib;
use App\Lib\oracle;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class BroadcastJMTOModulesController extends Controller {

    // use RESTActions;

    private $lib, $oracle;

    public function __construct()
    {
        $this->lib = new Lib();
        $this->oracle = new oracle();
    }

    public function cekTableSpace()
    {
        $cabang   = Cabang::all();
        $messageT = "Daftar cabang dengan usage table space status > 80% :";
        $messageP = "Cabang lain yg berhasil dicek :";

        try
        {
            foreach ($cabang as $key => $value)
            {
                if ($dataCabang = $cabang->where('nama_cabang', strtoupper($value['nama_cabang']))
                    ->where('status', 1)->first())
                {
                    Config::set('database.connections.oracle.host', $dataCabang['host']);
                    Config::set('database.connections.oracle.port', $dataCabang['port']);
                    Config::set('database.connections.oracle.username', $dataCabang['username']);
                    Config::set('database.connections.oracle.password', $dataCabang['password']);
                    Config::set('database.connections.oracle.service_name', $dataCabang['sid']);

                    $query = "select * from (
                        SELECT df.tablespace_name tablespace_name, MAX (df.autoextensible) auto_ext, ROUND (df.maxbytes / (1024 * 1024), 2) max_ts_size,
                               ROUND ((df.bytes - SUM (fs.bytes)) / (df.maxbytes) * 100, 2) max_ts_pct_used, ROUND (df.bytes / (1024 * 1024), 2) curr_ts_size, ROUND ((df.bytes - SUM (fs.bytes)) / (1024 * 1024), 2) used_ts_size,
                               ROUND ((df.bytes - SUM (fs.bytes)) * 100 / df.bytes, 2) ts_pct_used, ROUND (SUM (fs.bytes) / (1024 * 1024), 2) free_ts_size, NVL (ROUND (SUM (fs.bytes) * 100 / df.bytes), 2) ts_pct_free
                        FROM dba_free_space fs,
                             (
                                 SELECT tablespace_name, SUM (bytes) bytes, SUM (DECODE (maxbytes, 0, bytes, maxbytes)) maxbytes,
                                        MAX (autoextensible) autoextensible
                                 FROM dba_data_files
                                 GROUP BY tablespace_name
                             ) df
                        WHERE fs.tablespace_name(+) = df.tablespace_name
                        GROUP BY df.tablespace_name, df.bytes, df.maxbytes
                        UNION ALL
                        SELECT df.tablespace_name tablespace_name, MAX (df.autoextensible) auto_ext, ROUND (df.maxbytes / (1024 * 1024), 2) max_ts_size,
                               ROUND ((df.bytes - SUM (fs.bytes)) / (df.maxbytes) * 100, 2) max_ts_pct_used, ROUND (df.bytes / (1024 * 1024), 2) curr_ts_size, ROUND ((df.bytes - SUM (fs.bytes)) / (1024 * 1024), 2) used_ts_size,
                               ROUND ((df.bytes - SUM (fs.bytes)) * 100 / df.bytes, 2) ts_pct_used, ROUND (SUM (fs.bytes) / (1024 * 1024), 2) free_ts_size, NVL (ROUND (SUM (fs.bytes) * 100 / df.bytes), 2) ts_pct_free
                        FROM (
                                 SELECT tablespace_name, bytes_used bytes
                                 FROM V$" . "temp_space_header
                                 GROUP BY tablespace_name, bytes_free, bytes_used
                             ) fs,
                             (
                                 SELECT tablespace_name, SUM (bytes) bytes, SUM (DECODE (maxbytes, 0, bytes, maxbytes)) maxbytes,
                                        MAX (autoextensible) autoextensible
                                 FROM dba_temp_files
                                 GROUP BY tablespace_name
                             ) df
                        WHERE fs.tablespace_name(+) = df.tablespace_name
                        GROUP BY df.tablespace_name, df.bytes, df.maxbytes )
                        where max_ts_pct_used > 80
                        order by 1";
                    $fileName = "TableSpace_" . $value['nama_cabang'];

                    try
                    {
                        DB::connection('oracle')->getPdo();
                    }
                    catch (\Exception $e)
                    {
                        $messageP =
                            $messageP . "\n - " .
                            strtoupper($value['nama_cabang']) . "\n   > " . $e->getMessage();
                    }

                    try
                    {
                        if ($response = DB::connection('oracle')->select($query))
                        {
                            if (count($response) != 0)
                            {
                                if ((new TableSpaceExport($response))->store($fileName . ".xlsx"))
                                {
                                    $messageT =
                                        $messageT . "\n - " .
                                        strtoupper($value['nama_cabang']) . "\n   > " . "http://telebot.jmto.co.id/download/" . $fileName;
                                }
                                else
                                {
                                    $messageT =
                                        $messageT . "\n - " .
                                        strtoupper($value['nama_cabang']) . "\n   > " . "Generate excel failed";
                                }
                            }
                            else{
                                $messageP =
                                    $messageP . "\n - " . strtoupper($value['nama_cabang']);
                            }
                        } else {
                            $messageP =
                                $messageP . "\n - " . strtoupper($value['nama_cabang']);
                        }
                    } catch (\Exception $e) {
                        $messageP =
                            $messageP . "\n - " .
                            strtoupper($value['nama_cabang']) . "\n   > " . $e->getMessage();
                    }
                } else {
                    $messageP =
                        $messageP . "\n - " .
                        strtoupper($value['nama_cabang']) . "\n   > " . "tidak ditemukan";
                }

                Config::set('database.connections.oracle.host', "");
                Config::set('database.connections.oracle.port', "");
                Config::set('database.connections.oracle.username', "");
                Config::set('database.connections.oracle.password', "");
                Config::set('database.connections.oracle.service_name', "");
            }

            $this->lib->responseBroadcast(FALSE, $messageT . "\n---\n" . $messageP, FALSE, FALSE);
        } catch (\Exception $e) {
            $this->lib->responseBroadcast(FALSE, $e->getMessage(), FALSE, FALSE);
        }

        return "true";
    }
}
