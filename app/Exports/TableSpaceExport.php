<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class TableSpaceExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    use Exportable;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'TABLESPACE_NAME',
            'AUTO_EXT',
            'MAX_TS_SIZE',
            'MAX_TS_PCT_USED',
            'CURR_TS_SIZE',
            'USED_TS_SIZE',
            'TS_PCT_USED',
            'FREE_TS_SIZE',
            'TS_PCT_FREE'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event)
            {
                $headerCell = 'A1:I1';
                $event->sheet->getDelegate()->getStyle($headerCell)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerCell)->getAlignment()->setHorizontal('center');
                $event->sheet->getDelegate()->getStyle($headerCell)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('eceff1');
            }
        ];
    }
}