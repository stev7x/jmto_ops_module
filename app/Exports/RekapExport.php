<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class RekapExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    use Exportable;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            'TGL_REPORT', 'KODE_CABANG', 'KODE_GERBANG',
       'NAMA_GERBANG', 'SHIFT', 'MDRI_L_AT4',
       'MDRI_P_AT4', 'BBRI_L_AT4', 'BBRI_P_AT4',
       'BBNI_L_AT4', 'BBNI_P_AT4', 'BBCA_L_AT4',
       'BBCA_P_AT4', 'TOP_L_AT4', 'TOT_P_AT4',
       'MDRI_L_JN', 'MDRI_P_JN', 'BBRI_L_JN',
       'BBRI_P_JN', 'BBNI_L_JN', 'BBNI_P_JN',
       'BBCA_L_JN', 'BBCA_P_JN',  'TOT_L_JN',
       'TOT_P_JN', 'MDRI_L_RJ', 'MDRI_P_RJ',
       'BBRI_L_RJ', 'BBRI_P_RJ', 'BBNI_L_RJ',
       'BBNI_P_RJ', 'BBCA_L_RJ', 'BBCA_P_RJ',
       'TOP_L_RJ','TOT_P_RJ', 'MDRI_L_JNRJ',
       'MDRI_P_JNRJ', 'BBRI_L_JNRJ', 'BBRI_P_JNRJ',
       'BBNI_L_JNRJ', 'BBNI_P_JNRJ', 'BBCA_L_JNRJ',
       'BBCA_P_JNRJ', 'TOT_L_JNJR', 'TOT_P_JNRJ',
       'MDRI_L_FS', 'MDRI_P_FS', 'BBRI_L_FS',
       'BBRI_P_FS', 'BBNI_L_FS', 'BBNI_P_FS',
       'BBCA_L_FS', 'BBCA_P_FS', 'TOT_L_FS',
       'TOT_P_FS', 'MDRI_L_RFSOK', 'MDRI_P_RFSOK',
       'BBRI_L_RFSOK', 'BBRI_P_RFSOK', 'BBNI_L_RFSOK',
       'BBNI_P_RFSOK', 'BBCA_L_RFSOK', 'BBCA_P_RFSOK',
       'TOP_L_RFSOK','TOT_P_RFSOK', 'MDRI_L_RFSNOK',
       'MDRI_P_RFSNOK', 'BBRI_L_RFSNOK', 'BBRI_P_RFSNOK',
       'BBNI_L_RFSNOK', 'BBNI_P_RFSNOK', 'BBCA_L_RFSNOK',
       'BBCA_P_RFSNOK', 'TOT_L_RFSNOK','TOT_P_RFSNOK', 'list_reject'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event)
            {
                $headerCell = 'A1:BX1';
                $event->sheet->getDelegate()->getStyle($headerCell)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerCell)->getAlignment()->setHorizontal('center');
                $event->sheet->getDelegate()->getStyle($headerCell)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('eceff1');
            }
        ];
    }
}