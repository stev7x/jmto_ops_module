<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class AT4Export implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    use Exportable;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        return [
            '#',
            'TGL_REPORT',
            'IDCABANG',
            'IDGERBANG',
            'SHIFT',
            'TOTAL_TRANSAKSI',
            'TOTAL_RUPIAH'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event)
            {
                $headerCell = 'A1:G1';
                $event->sheet->getDelegate()->getStyle($headerCell)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($headerCell)->getAlignment()->setHorizontal('center');
                $event->sheet->getDelegate()->getStyle($headerCell)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('eceff1');
            }
        ];
    }
}