<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/download/{filename}', function ($filename)
{
//    $name = pathinfo($filename, PATHINFO_FILENAME);
//    $ext = pathinfo($filename, PATHINFO_EXTENSION);
//    return $name . $ext;
    return response()->download(storage_path("app/" . $filename . '.xlsx'));
});

/**
 * Routes for resource cabang
 */
$router->get('cabang', 'CabangsController@all');


/**
 * Routes for resource jmto-module
 */
$router->get('jmto/at4', 'JmtoModules@at4');
$router->get('jmto/cekfs', 'JmtoModules@cekFS');
$router->get('jmto/cekkonsistensi', 'JmtoModules@cekKonsistensi');
$router->get('jmto/datacabang', 'JmtoModules@dataCabang');
$router->get('jmto/antrian', 'JmtoModules@antrian');
$router->get('jmto/rekap', 'JmtoModules@rekap');
$router->get('jmto/rekapallshift', 'JmtoModules@rekapAllShift');
$router->get('jmto/cektablespace', 'JmtoModules@cekTableSpace');
$router->get('jmto/cektablespacestatus', 'JmtoModules@cekTableSpaceStatus');
$router->get('jmto/allcabang', 'JmtoModules@allCabang');
$router->get('broadcast/cektablespace', 'BroadcastJMTOModulesController@cekTableSpace');

/**
 * Routes for resource broadcast-j-m-t-o-modules
 */
